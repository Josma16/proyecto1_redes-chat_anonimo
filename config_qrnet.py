import configparser
import sys

config_file = '/home/jose/Documentos/Pycharm/proyecto1_redes-chat_anonimo/config.ini'

class ConfigQRNet():

    #Config of transmission device
    TRANSMISSION_DEVICE_IP = ""
    TRANSMISSION_PORT = 0

    #Config of server IRC
    SERVER_IRC_IP = ""
    LISTENING_IRC_PORT = 0

    #Config of net mesh
    REGISTRY_MESH_PORT = 0
    RECEIVING_MESH_PORT = 0
    INFO_MESH_PORT = 0
    NET_MESH_IP = ""
    DELETE_PORT = 0

    def __init__(self):

        config_parser = configparser.ConfigParser()

        if not config_parser.read(config_file):
            print("Can't read config file")
            sys.exit(1)


        config_transsmision_device = config_parser['TRANSMISSION_DEVICE']

        self.TRANSMISSION_DEVICE_IP = str(config_transsmision_device['TRANSMISSION_DEVICE_IP'])
        self.TRANSMISSION_PORT = int(config_transsmision_device['TRANSMISSION_PORT'])

        config_server_irc = config_parser['SERVER_IRC']

        self.SERVER_IRC_IP = str(config_server_irc['SERVER_IRC_IP'])
        self.LISTENING_IRC_PORT = int(config_server_irc['LISTENING_IRC_PORT'])

        config_net_mesh = config_parser['NET_MESH']

        self.REGISTRY_MESH_PORT = int(config_net_mesh['REGISTRY_MESH_PORT'])
        self.RECEIVING_MESH_PORT = int(config_net_mesh['RECEIVING_MESH_PORT'])
        self.INFO_MESH_PORT = int(config_net_mesh['INFO_MESH_PORT'])
        self.NET_MESH_IP = str(config_net_mesh['NET_MESH_IP'])
        self.DELETE_PORT = int(config_net_mesh['DELETE_PORT'])