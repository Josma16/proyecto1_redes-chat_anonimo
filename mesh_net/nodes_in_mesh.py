import random
import socket
from threading import Thread
import sys

sys.path.append("..")
import link_layer.node_qr as node_qr

from config_qrnet import ConfigQRNet

mesh_list = []

config = ConfigQRNet()

# Elimina un cliente de la red mesh 
def quit_client(client):
    mesh_list.remove(client)

# Agrega un nuevo nodo a la red con todos los atributos de entrada.
def new_node(ip_address, mac_address, port):
    new_n = node_qr.QRNode(mac_address, ip_address, port)
    mesh_list.append(new_n)

    print("Agregado exitosamente nodo con el puerto: " + port)

# Verifica que el cliente que desea ingresar no esté en la red actualmente 
def already_exist(ip_address, mac_address, port):
    for node in mesh_list:
        if node.mac_address == mac_address:
            return "a"
        elif node.ip_address == ip_address and node.port == port:
            return "b"

    return "c"

# Función encargada de interpretar el mensaje del CLIENTE que busca integrarse a la red.
# Encargada también de comunicar si la inserción fue un éxito.
def register_new_node(ip, connection):
    received_data = connection.recv(2048).decode("UTF-8")
    data_list = received_data.split(' ')

    if data_list[0] == 'MAC' and data_list[2] == 'PORT':
        mac_node = data_list[1]
        ip_node = ip
        port_node = data_list[3]

        response_exist = already_exist(ip_node, mac_node, port_node)
        if response_exist == 'a':
            connection.send('DULPICATEM'.encode('ascii'))
        elif response_exist == 'b':
            connection.send('DULPICATEP'.encode('ascii'))
        else:
            connection.send('OK/200'.encode('ascii'))
            new_node(ip_node, mac_node, port_node)


    else:
        connection.send('ERROR'.encode('ascii'))
    connection.close()


def mesh_server():
    s_mesh = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s_mesh.bind((config.NET_MESH_IP, config.REGISTRY_MESH_PORT))
    s_mesh.listen()

    # Se escuchan conexiones infinitamente
    while True:
        conn, address = s_mesh.accept()

        print("\n\nNuevo nodo en la red " + address[0])

        start = Thread(target=register_new_node, args=(address[0], conn))
        start.start()
        start.join()

# Función que recibe solicitudes de información de otros CLIENTES para poder crear
# las rutas de reenvío.
def info_mesh(conn):
    mac = conn.recv(2048).decode('UTF-8')
    index = [find_index_by_mac(mac)]
    client = find_node_by_index(index)

    ip = client[0].ip_address
    port = client[0].port

    info = ip + ' ' + port
    conn.send(info.encode('ascii'))
    conn.close()

# Recibe solicitudes de eliminación de CLIENTES
def del_mesh(conn):
    print(mesh_list)
    mac = conn.recv(2048).decode('UTF-8')
    index = [find_index_by_mac(mac)]
    client = find_node_by_index(index)
    mesh_list.remove(client[0])
    print(mesh_list)
    conn.close()

# Función que recibe una lista de índices los cuales buscará entre todos los clientes
#  y devolverá una lista con los datos de conectividad de los CLIENTES asociados 
# a los índices.
def find_node_by_index(index_list):
    clients = []
    for index in index_list:
        clients.append(mesh_list[index])
    return clients

# Busca los índices de los CLIENTES según la mac address de entrada y devuelve 
# el índice asociado.
def find_index_by_mac(mac_address):
    required_index = -1
    size = len(mesh_list)
    for i in range(size):
        check = mesh_list[i].mac_address
        if str(check) == str(mac_address):
            required_index = i
    return required_index


# Función que decide cómo armar la ruta de envío mediante la red mesh.
def sending_path(mc_origin, mc_dest):
    # Tenemos dos opcciones: tenemos muchos nodos y podemos generar un camino aleatorio o
    # contamos con pocos nodos por lo que ocultar el camino del mensaje es irrelevante
    if (len(mesh_list) - 1) > 3:
        path = random_path(mc_origin, mc_dest)
    else:
        path = normal_path(mc_origin, mc_dest)
    return path

# Genera una ruta aleatoria entre origen y destino para evitar saber con exactitud de donde 
# viene el mensaje.
def random_path(origin, destiny):
    index_list = []
    origin_index = find_index_by_mac(origin)
    destiny_index = find_index_by_mac(destiny)
    # Se le resta 1 para no contar el nodo origen
    if len(mesh_list) - 1 < 3:
        # Con menos de tres nodos de salto es mejor no realizar el procedimiento
        return None
    i = 0
    while i < 3:
        random_index = random.randint(0, len(mesh_list) - 1)

        if random_index != origin_index and random_index != destiny_index:
            if random_index not in index_list:
                index_list.append(random_index)
                i += 1

    nodes_list = find_node_by_index(index_list)
    nodes_list.append(mesh_list[destiny_index])
    return nodes_list


# A falta de nodos se procede a realizar un camino con lo que haya en la red mesh.
def normal_path(origin, dest):
    index_list = []
    origin_index = find_index_by_mac(origin)
    destiny_index = find_index_by_mac(dest)

    for i in range(len(mesh_list) - 1):
        if i != origin_index and i != destiny_index:
            index_list.append(i)
    nodes_list = find_node_by_index(index_list)
    nodes_list.append(mesh_list[destiny_index])
    return nodes_list


def find_path(conn):
    message_data = conn.recv(2048).decode('UTF-8')
    message_data = message_data.split(' ')

    if message_data[0] != 'SRC':
        print("No se recibió la mac del origen")

    elif message_data[2] != 'DST':
        print("No se recibió la mac del destinatario")

    else:
        mc_origin = message_data[1]
        mc_destiny = message_data[3]
        if find_index_by_mac(mc_destiny) == -1:
            print('No se ha encontrado el nodo destino: ' + mc_destiny)
            message = 'UNREGISTERED'
        else:
            node_list = sending_path(mc_origin, mc_destiny)
            message = 'PATH ' + " ".join(str(x) for x in node_list)
            print(message)
        conn.send(message.encode("ascii"))
    conn.close()

# Buzón de entrada para quienes desean enviar un mensaje por la red mesh.
def listening_messages():
    socket_main = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_main.bind((config.NET_MESH_IP, config.RECEIVING_MESH_PORT))
    socket_main.listen()

    while True:
        conn, address = socket_main.accept()
        listening = Thread(target=find_path, args=(conn,))
        listening.start()
        listening.join()

# Buzón que verifica si alguien desea datos de algún CLIENTE en concreto. 
def info_box():
    socket_main = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_main.bind((config.NET_MESH_IP, config.INFO_MESH_PORT))
    socket_main.listen()

    while True:
        conn, address = socket_main.accept()
        inf = Thread(target=info_mesh, args=(conn,))
        inf.start()
        inf.join()

# Buzón que escucha si alguien desea borrarse de la red mesh.
def delete_request():
    socket_main = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_main.bind((config.NET_MESH_IP, config.DELETE_PORT))
    socket_main.listen()

    while True:
        conn, address = socket_main.accept()
        inf = Thread(target=del_mesh, args=(conn,))
        inf.start()
        inf.join()

def main():
    print('Servidor Mesh activo!')
    register = Thread(target=mesh_server)
    register.start()

    info = Thread(target=info_box)
    info.start()

    messages_server = Thread(target=listening_messages)
    messages_server.start()

    delete = Thread(target=delete_request)
    delete.start()

    register.join()
    messages_server.join()


if __name__ == '__main__':
    main()
