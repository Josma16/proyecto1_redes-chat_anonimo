++++JBJ PROTOCOL++++
     Junio 2020

---+RESUMEN PROTOCOLO+---

Protocolo de transferencia física mediante QR, chat anónimo 
en una red malla y conexión a servidor IRC mediante CLEARNET.

---+CAPAS QUE LO COMPONEN+---

*/* Capa Física: encargada de la representación de tramas, utilizando
                 códigos QR para su transmisión entre redes.

*/* Capa de Enlace y Red: a pesar de muchas veces ser tratadas como capas
                          independientes, en este caso se tratarán como
                          una misma. Encargada de la transmisión de datos 
                          entre clientes mediantes puertos de escucha de 
                          origen virtual. Además tiene como tarea administrar
                          las rutas que tomarán los mensajes en modo anónimo.
                          Con lo anterior se denota que está compuesta por los 
                          nodos inscritos a la red Mesh.

*/* Capa de Aplicación:  capa que permite la interacción del usuario con la red ya 
                         que es la interfaz que llama la lógica del resto de capas 
                         para lograr la transmisión a través de la red Mesh o Clearnet.


---+COMPONENTES+---

Cualquier red que corra con el protocolo JBJ contará con un DISPOSITIVO DE TRANSMISIÓN, 
LECTOR DE QR, un SERVIDOR IRC,SERVIDOR MESH (Registro de clientes) y varios clientes afiliados a la red.

Cada cliente tiene la oportunidad de elegir a quien re-transmitir su mensaje y bajo qué canal  
de transmisión del protocolo. A continuación cada componente mejor explicado: 
    
CLIENTE:  Este componente es capaz de unirse a un servidor IRC, enviar mensajes en la red malla anónima 
en el mismo servidor registrado o generar mensajes en códigos QR para enviar a una red malla en otro servidor.

SERVIDOR IRC: Este servidor provee los servicios del chat IRC, se encarga de crear diferentes canales y 
permitir el intercambio de mensajes entre miembros del mismo canal. Cada mensaje que se reciba en este 
servidor va a ser reenviando por CLEARNET en difusión a todos los miembros del mismo canal.

SERVIDOR MESH:  Este servidor es el encargado del manejo de los clientes en la red malla, lleva un registro
de los mismo con sus dirección MAC, la IP y el puerto en el que se ha conectado. Es el componente que se
encarga de manejar el anonimato de los mensajes y el mecanismo de entrelazar varios clientes para que el
mensaje vaya saltando entre ellos hasta llegar al destino. En caso de que el mensaje sea proveniente de 
un QR lo envía directamente a la MAC destino que se recibe en el mensaje.

DISPOSITIVO DE TRANSMISIÓN: Este componente tiene el trabajo relacionado a la creación de códigos QR,
puede realizar esto tomando un archivo o bien un mensaje de texto escrito por el usuario.

LECTOR DE QR: Componente encargado de la lectura de los códigos QR, es capaz de leer archivos QR desde 
un directorio determinado o bien desde la cámara del dispositivo donde está corriendo. 

---+FUNCIONAMIENTO+---

Para el correcto funcionamiento de los clientes debe estar corriendo un SERVIDOR MESH, SERVIDOR IRC y 
un DISPOSITIVO DE TRANSMISIÓN. Durante la explicación del funcionamiento se va a detallar las acciones asociadas 
con cada acción realizada por el  cliente.

Para iniciar un cliente de debe escribir el número del puerto en que se va a estar conectando al SERVIDOR MESH, 
después de esto se presenta un menú el cual muestra en el encabezado la dirección MAC asociada a la sesión en curso 
así como las opciones de crear un nuevo QR, entrar a un servidor IRC, enviar un mensaje a través del SERVIDOR MESH o 
salir de la sesión.

A continuación se detallan las opciones que se presentan al usuario y su relación con los componentes

1.Crear QR nuevo: Esta función hace uso del DISPOSITIVO DE TRANSMISIÓN para crear códigos QR. El servicio 
va a consultar al usuario si desea enviar un mensaje de texto o bien un archivo. En caso de querer enviar 
un archivo se pide la ruta exacta en donde se encuentra el mismo (en el dispositivo que se esté corriendo) 
y la dirección MAC del usuario al que se quiere enviar el mensaje. Si lo que se desea enviar es un mensaje 
de texto se debe escribir el mensaje y de igual manera ingresar la dirección MAC del destino. Como salida 
de este proceso se tiene una carpeta con el nombre <nombre_archivo_a_enviar>QRs (en caso de querer enviar un archivo)
en la cual se encuentra el o los QRs generados (con el nombre QRi.png, donde 0<= i < QRs_generados) o bien un archivo
llamado “Message to: <MAC_destino>” en caso de que se quiera enviar un mensaje de texto.El formato de la trama que se 
va a almacenar en los QR es: MAC,tipo[t, f],protocol_version,checksum,data = 103 bytes disponibles. Un ejemplo de esta 
trama sería 123456789101,t,0.1,161618,data (donde data es el mensaje, un código hexadecimal en caso de ser archivos o 
los caracteres en caso de ser texto). Se usa “t” cuando se enviar un mensaje de texto y “f” cuando es un archivo.

2.Entrar al servidor IRC: Para esta función debe estar corriendo el SERVIDOR IRC. Se le va a solicitar al cliente un 
“nickname” el cual va a ser su identificación en el chat, seguidamente el nombre del canal al que desea ingresar, 
si el canal no existe se va a crear y en caso de existir se va a unir a este. Al escribir /EXIT va a salir del canal 
en el que se encuentra.

3.Enviar mensaje en la red Mesh: Es fundamental que el SERVIDOR MESH se encuentre activo para realizar esta acción. 
Una vez el usuario ingrese esta opción el sistema la va a solicitar que escriba su mensaje usando el formato 
“SEND <mac_destino> <mensaje a enviar>” (sin las comillas) una vez hecho esto se va a proceder a enviar el mensaje 
por medio de la red mesh. El uso de la palabra SEND sólo anuncia la llegada de un mensaje, pero una vez verificado 
arma un nuevo paquete de información de tal manera: “SRC <mac_origen> DST <mac_destino>”, donde SRC es la cabecera 
de la mac del emisor y DST la cabecera de la mac receptora. Esto es enviado al servidor mesh donde verifica que se 
hayan enviado ambos datos y luego se termina de validar si la mac destino realmente existe en la red mesh. 
Una vez haya pasado por todas las validaciones anteriores el SERVIDOR MESH se dispone a crear una ruta de envío la 
cual depende de la cantidad de CLIENTES registrados en la red mesh. Si la red cuenta con más de 3 CLIENTES entonces 
será capaz de crear una ruta aleatoria entre todos los nodos de salto con lo cual se oculta de dónde proviene el mensaje; 
si hay 3 o menos CLIENTES no es necesario ocultar el mensaje debido a que las opciones de quien puede ser el emisor son 
pocas por lo que se genera un envío al CLIENTE destino de un sólo salto. Volviendo al caso de camino aleatorio va a tomar 
de forma aleatoria 3 CLIENTES para que funcionen de nodos de salto (sin contar al origen y el destino por supuesto), una 
vez seleccionados se agrega la información de envío del destino al final de una lista que contiene todos los CLIENTES de 
la ruta creada. Según la cantidad de nodos en la ruta se va a crear un paquete de transporte de datos que les informará 
cuando deben reenviar el mensaje(y a quién) y cuando el mensaje va dirigido a ellos, por lo tanto tenemos lo siguiente: 
“RESEND <ip_nodo> <puerto_nodo> … MESSAGE <mensaje_enviado>” donde RESEND es la palabra clave para reenviar el mensaje a 
los datos que le siguen y antes de enviar el paquete nuevamente retira esa capa de reenvío, los puntos suspensivos hacen 
alusión a que pueden venir tantas combinaciones RESEND como CLIENTES formen la ruta de envío (sin contar el CLIENTE destino) y 
por último quien sólo recibe la palabra clave MESSAGE será quien pueda mostrar al usuario lo que contenga <mensaje_enviado>. 
Al final el único nodo de salto que no puede haber enviado el mensaje es quien realiza la conexión con el CLIENTE destino, 
el resto de nodos o CLIENTES son posibles emisores del mensaje pero eso no lo podrá averiguar el receptor.

4.Salir: Esta opción manda una petición al SERVIDOR MESH para que la MAC del usuario sea retirada del sistema.

Para la lectura de un QR se debe ejecutar el LECTOR DE QR, este servicio ofrece la posibilidad de leer los QR desde 
algún directorio en caso de que los QR hayan sido generados a partir de un archivo o bien desde la cámara, para mensajes 
de texto únicamente se pueden leer QR desde la cámara del dispositivo en que se está corriendo el servicio. Una vez leídos 
los datos son enviados por medio del SERVIDOR MESH a la MAC destino que detalla la información en los código QR. Para capturar 
un QR con la cámara se debe presionar la tecla c en el teclado, al tener todos los QR capturados se debe presionar la tecla Esc.
