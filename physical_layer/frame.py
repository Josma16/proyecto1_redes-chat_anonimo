
class Frame:
    PROTOCOL_VERSION = '0.1'
    CHECKSUM = '161618'

    def new_frame(self, mac_address, type, message):
        '''
        MAC,type[t, f],protocol_version,checksum,data = 103 available bytes in QR cases
        123456789101,t,0.1,161618,data
        '''

        new_frame = mac_address + ',' + type + ',' + self.PROTOCOL_VERSION + ',' + self.CHECKSUM + ',' + str(message)

        return new_frame




