import socket
import sys
from threading import Thread

sys.path.append("..")
from qr_utils import qr_generator
from frame import Frame
from config_qrnet import ConfigQRNet

config = ConfigQRNet()


class trans_device:

    def server(self):
        s_qr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s_qr.bind((config.TRANSMISSION_DEVICE_IP, config.TRANSMISSION_PORT))
        s_qr.listen()

        # Se escuchan conexiones infinitamente
        while True:
            conn, address = s_qr.accept()

            print("\n\nNueva solicitud QR de " + address[0])

            s_trans = Thread(target=self.new_transmition, args=(conn,))
            s_trans.daemon = True
            s_trans.start()
            s_trans.join()


    def new_transmition(self, connection):
        frame = Frame()
        generator = qr_generator()
        recv_message = connection.recv(2048).decode("UTF-8")
        recv_message = recv_message.split(' ')
        print(recv_message)

        mac_dest = recv_message[1]
        action = recv_message[2]
        data = recv_message[3]
        frame_to_qr = frame.new_frame(mac_dest, action, data)

        if action == "t":
            generator.generate_qr(data=frame_to_qr, qr_name='Message to: ' + mac_dest)
        else:
            generator.file_to_qr(mac_dest, bytes_to_read=103, filename=data)

        print("Si llegó")
        connection.close()


def main():
    service = trans_device()
    service.server()


if __name__ == "__main__":

    try:
        print("Transmission device activo!")
        print((config.TRANSMISSION_DEVICE_IP, config.TRANSMISSION_PORT))
        main()
    except KeyboardInterrupt as e:
        print('[+] Bye!')
